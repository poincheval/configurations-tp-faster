# Configurations TP FASTER

Ce dépôt contient des configurations pour les TP de physique des salles S2-307 et S2-309.

Les configurations peuvent être téléchargées en utilisant faster_activity,
et le TP démarré avec ./start-activity.


## note pour les « propriétaires » du dépôt (les administrateurs)

Nous utilisons le dépôt uniquement sur la branche 'main' :
les utilisateurs souhaitant modifier le dépôt (créer des TP) doivent être
enregistrés comme des 'mainteneurs' (la branche 'main' est protégée, il faut
être mainteneur pour écrire dedans).


## modèles de cartes FASTER

À l'heure actuelle, deux modèles de cartes FASTER sont utilisés en S2-309 :
 * cartes v2 (MOSAHR)
 * cartes c5 (CARAS)

Il est donc nécessaire de créer chaque TP dans deux versions différentes, avec un identifiant suffixé par '_v2' ou '_c5'.


## créer une configuration ou modifier les configurations existantes

Soit un utilisateur « demouser » souhaitant créer une nouvelle configuration pour un TP nommé « tp_instructif ».


### pré-requis

 * disposer d'un compte sur le gitlab de l'université, https://git.unicaen.fr
 * avoir défini un mode d'authentification sur votre compte gitlab :
	* soit mot de passe pour publier en HTTP
	* soit une clé SSH
 * faire partie des mainteneurs du projet


### cloner le dépôt


```
demouser@userpc:~$ mkdir tp-git
demouser@userpc:~$ cd tp-git/
demouser@userpc:~/tp-git$ git clone https://git.unicaen.fr/poincheval/configurations-tp-faster.git
Clonage dans 'configurations-tp-faster'...
remote: Enumerating objects: 71, done.
remote: Counting objects: 100% (31/31), done.
remote: Compressing objects: 100% (30/30), done.
remote: Total 71 (delta 9), reused 0 (delta 0), pack-reused 40
Réception d'objets: 100% (71/71), 17.91 Kio | 2.56 Mio/s, fait.
Résolution des deltas: 100% (25/25), fait.
```

### se placer dans un nouveau répertoire ```tp_instructif``` dans l'arborescence ```configurations-tp-faster```

```
demouser@userpc:~/tp-git$ cd configurations-tp-faster/
```

S'il s'agit d'un nouveau TP, créer le répertoire :

```
demouser@userpc:~/tp-git/configurations-tp-faster$ mkdir tp_instructif
```

> **_Note_** Encore une fois, il est probablement souhaitable de créer par
exemple les deux répertoires :
 * tp_instructif_v2
 * tp_instructif_c5

et de créer le TP pour les deux architectures de cartes.

Se placer dans le répertoire:

```
demouser@userpc:~/tp-git/configurations-tp-faster$ cd tp_instructif/
```

### créer la configuration avec ```faster_setup_gui```

```
demouser@userpc:~/tp-git/configurations-tp-faster/tp_instructif$ faster_setup_gui 
no cfg directory
V2 does not have a .cfg file
==> The sample RHB configuration files will be updated or created

==> The RHB sample configuration has been created
    Many spectra have been created based on your faster configuration
    This is an example and you can add your own spectra
    If you don't know how to declare a new spectra, many example have been created.
==> You can install them with apt command : 
    For example : sudo apt install faster-rhb-demo-qdc will install the qdc demo package
                  faster_rhb_demo_qdc_copy will install a copy of all examples you 
                    will need to create and display spectra for a qdc channel

==> The RHB sample configuration has been created
==> This shell is able to display many rhb template

demouser@userpc:~/tp-git/configurations-tp-faster/tp_instructif$ ll
total 36
drwxrwxr-x 3 demouser demouser 4096 mars   8 16:53 ./
drwxrwxr-x 7 demouser demouser 4096 mars   8 16:50 ../
drwxrwxr-x 2 demouser demouser 4096 mars   8 16:53 cfg/
-rw-rw-r-- 1 demouser demouser  317 mars   8 16:53 .RHBrc
-rw-rw-r-- 1 demouser demouser 7173 mars   8 16:53 sample.facqConf
-rw-rw-r-- 1 demouser demouser  300 mars   8 16:53 sample.pid
-rw-rw-r-- 1 demouser demouser 4635 mars   8 16:53 sample.rhvConf
```


> **_Note_** dans l'immédiat, recopier les scripts 
 * rhb-user.pre-start.sh
 * rhb-user.post-stop.sh
 * start-activity

d'un autre TP. Une prochaine mise-à-jour automatisera cette étape.


### modifier les réglages d'un TP existant

Faire les réglages avec ```faster_gui```, penser à sauver la configuration
des canaux une fois le TP opérationnel.


### revenir dans le répertoire ```configurations-tp-faster```


```
demouser@userpc:~/tp-git/configurations-tp-faster/tp_instructif$ cd ..
demouser@userpc:~/tp-git/configurations-tp-faster$ ll
total 36
drwxrwxr-x 7 demouser demouser 4096 mars   8 16:50 ./
drwxrwxr-x 3 demouser demouser 4096 mars   8 16:49 ../
drwxrwxr-x 3 demouser demouser 4096 mars   8 16:49 CsI_ADC_direct/
drwxrwxr-x 8 demouser demouser 4096 mars   8 16:53 .git/
-rw-rw-r-- 1 demouser demouser    3 mars   8 16:49 .gitignore
-rw-rw-r-- 1 demouser demouser  245 mars   8 16:49 README.md
drwxrwxr-x 3 demouser demouser 4096 mars   8 16:49 tp_carte_c5_caras/
drwxrwxr-x 3 demouser demouser 4096 mars   8 16:49 tp_carte_v2_mosahr/
drwxrwxr-x 3 demouser demouser 4096 mars   8 16:53 tp_instructif/
```


### ajouter le répertoire contenant le nouveau TP

```
demouser@userpc:~/tp-git/configurations-tp-faster$ git add tp_instructif/
demouser@userpc:~/tp-git/configurations-tp-faster$ git status 
Sur la branche main
Votre branche est à jour avec 'origin/main'.

Modifications qui seront validées :
  (utilisez "git restore --staged <fichier>..." pour désindexer)
	nouveau fichier : tp_instructif/.RHBrc
	nouveau fichier : tp_instructif/cfg/ch_1_2_3_4.cfg
	nouveau fichier : tp_instructif/cfg/config_faster.yaml
	nouveau fichier : tp_instructif/cfg/faster_trunk.cfg
	nouveau fichier : tp_instructif/sample.facqConf
	nouveau fichier : tp_instructif/sample.pid
	nouveau fichier : tp_instructif/sample.rhvConf
```

### synchroniser votre répertoire

```
demouser@userpc:~/tp-git/configurations-tp-faster$ git pull
```

Si vous rencontrez des erreurs à cette étape, et que vous ne connaissez pas
git... consultez un collègue (ceci n'est pas une plaisanterie, cette doc ne
peut pas se substituer à un vrai cours sur git).


### « committer » (valider ?) l'ajout de ces fichiers

```
demouser@userpc:~/tp-git/configurations-tp-faster$ git commit
```

Votre éditeur de fichier s'ouvre, avec une liste des modifications que vous vous apprêtez à valider.
Saisir (obligatoirement) une description pour ces modifications (ici, « un TP fort instructif pour les étudiants en master », puis quitter l'éditeur en enregistrant le texte.

```
un TP fort instructif pour les étudiants en master
# Veuillez saisir le message de validation pour vos modifications. Les lignes
# commençant par '#' seront ignorées, et un message vide abandonne la validation.
#
# Sur la branche main
# Votre branche est à jour avec 'origin/main'.
#
# Modifications qui seront validées :
#       nouveau fichier : tp_instructif/.RHBrc
#       nouveau fichier : tp_instructif/cfg/ch_1_2_3_4.cfg
#       nouveau fichier : tp_instructif/cfg/config_faster.yaml
#       nouveau fichier : tp_instructif/cfg/faster_trunk.cfg
#       nouveau fichier : tp_instructif/sample.facqConf
#       nouveau fichier : tp_instructif/sample.pid
#       nouveau fichier : tp_instructif/sample.rhvConf
#
```

### « pousser » le TP sur le serveur

```
demouser@userpc:~/tp-git/configurations-tp-faster$ git push
Username for 'https://git.unicaen.fr': <votre nom d'utilisateur gitlab>
Password for 'https://<votre nom d'utilisateur gitlab>@git.unicaen.fr': <votre mot de passe pour un push en HTTP>
Énumération des objets: 12, fait.
Décompte des objets: 100% (12/12), fait.
Compression par delta en utilisant jusqu'à 8 fils d'exécution
Compression des objets: 100% (11/11), fait.
Écriture des objets: 100% (11/11), 4.15 Kio | 4.15 Mio/s, fait.
Total 11 (delta 1), réutilisés 4 (delta 0), réutilisés du pack 0
To https://git.unicaen.fr/poincheval/configurations-tp-faster.git
   57a9904..4102559  main -> main
```


Votre TP est maintenant publié sur https://git.unicaen.fr/poincheval/configurations-tp-faster/.


## cas particuliers

Je ne pense pas que vous devriez utiliser ce qui suit...

### publier une modification sous son nom depuis le répertoire d'un étudiant

```
git commit --author="Votre Nom <votre@adresse.mail>" -m "Impersonation is evil."
```

Mais cette commande ne fonctionne que si le compte contient déjà une configuration
git. En cas d'échec, faire :

```
  git config --global user.email "account_owner@example.com"
  git config --global user.name "account owner name"
```

puis réessayer le ```git commit --author=...```.

